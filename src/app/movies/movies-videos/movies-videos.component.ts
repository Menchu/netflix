import { ApiResponse } from '../movies-list/models/movies-list-model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Videos } from './models/movies-videos-model';


import { MoviesVideosService } from './services/movies-videos.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-movies-videos',
  templateUrl: './movies-videos.component.html',
  styleUrls: ['./movies-videos.component.scss']
})
export class MoviesVideosComponent implements OnInit {
  public moviesVideos: Videos[];
  public movieId: string;



  constructor(private moviesVideosService: MoviesVideosService, private route: ActivatedRoute,
    private _sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.movieId = params.get('id');
    });
    this.moviesVideosService.getMoviesVideos(this.movieId).subscribe((data: Videos[]) => {

      this.moviesVideos = data;
    }, (err) => {
      console.error(err);
    });

  }
  getVideoIframe(url) {
    var video, results;

    if (url === null) {
      return '';
    }
    results = url.match('[\\?&]v=([^&#]*)');
    video = (results === null) ? url : results[1];

    return this._sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video);
  }


}



