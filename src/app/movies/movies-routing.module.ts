import { MoviesVideosComponent } from './movies-videos/movies-videos.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MoviesListComponent } from './movies-list/movies-list.component';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';


const routes: Routes = [
  { path: '', component: MoviesListComponent },
  {
    path: ':id',
    component: MoviesDetailsComponent,
  },
  {
    path: ':id/videos',
    component: MoviesVideosComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
