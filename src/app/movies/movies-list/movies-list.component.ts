import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { faSearch, faBars } from '@fortawesome/free-solid-svg-icons';

import { MoviesListService } from './services/movies-list.service';
import { Movies } from './models/movies-list-model';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  faSearch = faSearch;
  faBars = faBars;

  public postersMoviesList: Movies[]; // Listado de péliculas.
  public posterFilteredList: Movies[];
  public filter: string;

  constructor(private moviesListService: MoviesListService, private router: Router) {/* EMPTY */ }

  ngOnInit(): void {
    this.moviesListService.getPostersFilmList().subscribe((data: Movies[]) => {

      this.postersMoviesList = data;
      // console.log(data);

    }, (err: any) => {
      console.error(err);
    });
  }
  public onChangeFilter(filter: string): void {
    const newList: Movies[] = this.posterFilteredList.filter(element => element.title.toLowerCase().includes(filter.trim().toLowerCase()));
    this.posterFilteredList = newList;
  }
  public navigateTo(id: number): void {
    /* this.router.navigate([index]); */   /* esto es angular 9 y funciona */
    this.router.navigateByUrl('movies/' + id);  /* angular 10 y tambien funciona */
  }/* especificas la url por la que se va a navegar */
}
