import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ApiResponse, Movies, ApiMoviesNetflix } from '../models/movies-list-model';

@Injectable({
  providedIn: 'root'
})
export class MoviesListService {

  private baseUrl = 'https://api.themoviedb.org/3/movie/popular?api_key=';

  private key = 'f8da09545bcfbe3ebb9b87d6d72e5c19&language=en-US&page=1';
  constructor(private http: HttpClient) {/* EMPTY */ }

  public getPostersFilmList(): Observable<Movies[]> {

    const posterListObs: Observable<Movies[]> =
      this.http.get(this.baseUrl + this.key)
        .pipe(
          map((res: ApiMoviesNetflix) => {
            console.log(res);
            if (!res) {
              throw new Error('Not found');
            } else {
              const results: ApiResponse[] = res.results;

              const formattedPosterList = results.map(({ poster_path, id, title }) => ({

                poster_path: 'https://image.tmdb.org/t/p/w342/' + poster_path,
                id,
                title
              }));
              return formattedPosterList;
            }
          }),
          catchError(err => {
            throw new Error(err.message);
          })
        );
    return posterListObs;
  }
}
