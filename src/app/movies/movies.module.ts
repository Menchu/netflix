import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { MoviesListComponent } from './movies-list/movies-list.component';

import { MoviesListService } from './movies-list/services/movies-list.service';
import { MoviesDetailsService } from './movies-details/services/movies-details.service';
import { MoviesVideosService } from './movies-videos/services/movies-videos.service';
import { MovieListItemComponent } from './movies-list/movie-list-item/movie-list-item.component';
import { MoviesDetailsComponent } from './movies-details/movies-details.component';
import { MoviesVideosComponent } from './movies-videos/movies-videos.component';

import { SearchPipe } from 'src/app/pipe/search.pipe';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [
    MoviesComponent,
    MoviesListComponent,
    MovieListItemComponent,
    MoviesDetailsComponent,
    MoviesVideosComponent,
    SearchPipe
    ],

  imports: [
    CommonModule,
    MoviesRoutingModule,
    RouterModule,
    FormsModule,
    FontAwesomeModule,
  ],
  providers: [MoviesListService, MoviesDetailsService, MoviesVideosService],
})
export class MoviesModule { }

