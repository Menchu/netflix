import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ApiResponseDetails } from '../models/movies-details-model';

@Injectable({
  providedIn: 'root'
})
export class MoviesDetailsService {

  private baseUrl = 'https://api.themoviedb.org/3/';
  private queryMovies = 'movie/';
  private key = '?api_key=f8da09545bcfbe3ebb9b87d6d72e5c19';
  private queryLanguage = '&language=es';

  constructor(private http: HttpClient) { /* empty */ }

  public getMoviesDetails(movieId: string): Observable<ApiResponseDetails> {

    const moviesDetailsObs: Observable<ApiResponseDetails> =
      this.http.get(this.baseUrl + this.queryMovies + movieId + this.key + this.queryLanguage)
        .pipe(
          map((res: ApiResponseDetails) => {
            if (!res) {
              throw new Error('Not found details');
            } else {
              return res;

            }
          }),
          catchError(err => {
            throw new Error(err.message);
          })
        );
    return moviesDetailsObs;
  }

}