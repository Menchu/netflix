import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { MoviesDetailsService } from './services/movies-details.service';
import { ApiResponseDetails } from './models/movies-details-model';
import { faPlayCircle } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-movies-details',
  templateUrl: './movies-details.component.html',
  styleUrls: ['./movies-details.component.scss']
})
export class MoviesDetailsComponent implements OnInit {
  faPlayCircle = faPlayCircle;

  public moviesDetails: ApiResponseDetails;
  public movieId: string;



  constructor(private moviesDetailsService: MoviesDetailsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.movieId = params.get('id');
    });
    this.moviesDetailsService.getMoviesDetails(this.movieId).subscribe((data: ApiResponseDetails) => {

      this.moviesDetails = data;
    }, (err) => {
      console.error(err);
    });
  }
}
