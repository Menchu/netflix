import { Pipe, PipeTransform } from '@angular/core';
import { Movies } from '../movies/movies-list/models/movies-list-model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(allMovies: Movies[], ...args: unknown[]): Movies[] {
    const filteredList: string = args[0] as string;
    return allMovies.filter((poster: Movies) => {
      const posterUpperCase: string = poster.title.toUpperCase();
      const filteredListUpperCase: string = filteredList ? filteredList.toUpperCase() : '';
      return posterUpperCase.includes(filteredListUpperCase);
    });
  }
}
